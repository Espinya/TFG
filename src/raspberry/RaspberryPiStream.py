#Fuentesconsultadas
#https://picamera.readthedocs.io/en/release-1.10/recipes1.html
#https://stackoverflow.com/questions/53576851/socket-programming-in-python-using-pickle
#http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_gui/py_video_display/py_video_display.html

from picamera.array import PiRGBArray
from picamera import PiCamera
from time import sleep
import socket
import pickle
import threading
import os
from Arguments import Arguments
class RaspberryCamera:
    def __init__(self,port=8005,resolution=(640,480),fps=32, tcpBuffSize=8192000, nCaptures=0):
        self.__tcpBuffSize = tcpBuffSize
        self.__port=port
        self.__camera=None
        self.__rawCapture=None
        self.__connection=None
        self.__haveConnection=False
        self.__resolution=resolution
        self.__fps=fps
        self.__frame = b""
        self.__frameBuffer = []
        self.__nCaptures = nCaptures
        self.__socket=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__socket.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, self.__tcpBuffSize)
        print("starting...")
        self.startServer()
        self.startCamera()
        self.startSendDataThread()
        try:
            self.waitConnection()
            self.capture()
        except Exception as ex:
            print("Error",ex)
            self.closeConnection()
            os._exit(1)


    def startServer(self):
        self.__socket.bind(('0.0.0.0',self.__port))
        self.__socket.listen(1)
        print("Server Running")

    def startCamera(self):
        self.__camera=PiCamera()
        self.__camera.resolution=self.__resolution
        self.__camera.framerate=self.__fps
        self.__rawCapture=PiRGBArray(self.__camera,size=self.__resolution)
        sleep(0.1)
        print("Camera Running")

    def waitConnection(self):
        if self.__haveConnection==False:
            self.__connection,self.__address=self.__socket.accept()
            self.__connection.setblocking(True)
            self.__haveConnection=True
            print("Connection Established")

    def closeConnection(self):
        if self.__haveConnection==True:
            self.__haveConnection=False
            self.__socket.close()
            self.__camera.release()
            print("Connection Closed")

    def capture(self):
        if self.__haveConnection==True:
            print("Capturing")
            for frame in self.__camera.capture_continuous(self.__rawCapture,format="bgr",use_video_port=True):
                if self.__nCaptures == 0:
                    self.__frame = frame.array
                else:
                    self.__frameBuffer.append(frame.array)
                    if len(self.__frameBuffer) == self.__nCaptures:
                        print("Sending")
                        while True:
                            if len(self.__frameBuffer) > 0:
                                self.__frame = self.__frameBuffer.pop(0)
                                self.sendData()
                            else:
                                self.closeConnection()
                                os._exit(1)
                self.__rawCapture.seek(0)

    def sendData(self):
        try:
            while(True):
                if len(self.__frame) != 0:
                    data=pickle.dumps(self.__frame)
                    self.__frame = b''
                    self.__connection.sendall(data)
                    self.__connection.recv(1)

        except Exception as e:
            print("Error in Thread sendData", e)
            os._exit(1)

    def startSendDataThread(self):
       thread = threading.Thread(target=self.sendData, args=())
       thread.setDaemon(True)
       thread.start()
       print("Thread Running")


if __name__=="__main__":
    args = Arguments()
    project=RaspberryCamera(port=args.port, resolution=args.resolution, tcpBuffSize=args.tcpBuffSize)