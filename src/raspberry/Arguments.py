#!/usr/bin/python

import sys

#add fpsmax
class Arguments:
    def __init__(self, port = 8005, resolution = (640, 480), tcpBuffSize = 8192, nCaptures = 0):
        self.__port = port
        self.__resolution = resolution
        self.__tcpBuffSize = tcpBuffSize
        self.__nCaptures= nCaptures
        self.findParams()

    def findParams(self):
        for index, arg in enumerate(sys.argv):
            if str(arg)[0] == "-":
                if index == len(sys.argv)-1:
                    raise Exception("Param {} don't have value".format(arg))
                elif str(sys.argv[index+1])[0] == "-":
                     raise Exception("Param {} don't have value, just another param".format(arg))
                else:
                    self.setParam(arg, sys.argv[index+1])
    def setParam(self, param, value):
        if param == "-port":
            self.__port = int(value)
        elif param == "-resX":
            self.__resolution = (int(value) , self.__resolution[1])
        elif param == "-resY":
            self.__resolution = (self.__resolution[0], int(value))
        elif param == "-tcpBuffSize":
            self.__tcpBuffSize = int(value)
        elif param == "-nCaptures":
            self.__nCaptures= int(value) 
        else:
            raise Exception("Unknow param")
    
    @property
    def port(self):
        return self.__port
    @property
    def resolution(self):
        return self.__resolution
    @property
    def tcpBuffSize(self):
        return self.__tcpBuffSize
    @property
    def nCaptures(self):
        return self.__nCaptures
        
if __name__=="__main__":
    "python args.py -port 8005 -resX 640 -resY 480 -tcpBuffSize 8192000"
    

    params = Args()
    port = params.port
    resolution = params.resolution
    tcpBuffSize = params.tcpBuffSize
    port +=1
    print("Port: ", port)
    print("Resolution: ", resolution)
    print("tcpBuff: ", tcpBuffSize)
   
