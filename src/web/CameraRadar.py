import cv2
import numpy as np


class CameraRadar:
    def __init__(self, x = 0, y = 0, angle = 0, ref = 0):
        self.x = x
        self.y = y
        self.angle = angle
        self.ref = ref
    def isTheSameCamera(self, data):
        if self.x == data[1] and self.y == data[2]  and self.angle == data[3]:
            return True
    def checkChanges(self, data):
        if not self.isTheSameCamera(data):
            self.x = data[1]
            self.y = data[2]
            self.angle = data[3]
            return True

    def draw(self, image):
        x = int(self.ref*self.x/100)  
        y = int(self.ref*self.y/100)

        angle = self.angle
        for i in range(6):
            self.semiCircle(image,x,y, distance=self.ref*i, angleIn=angle-30, angleFin=angle+30)

        self.drayLine(image,x,y, distance=self.ref*5, angle=angle-30, color=(0, 0, 0))
        self.drayLine(image,x,y, distance=self.ref*5, angle=angle+30, color=(0, 0, 0))
        cv2.circle(image, (x, y), 3, (0, 0, 0), 2)

    def drayLine(self, image, x, y, distance, angle, color=(0,0,0)):
        xf, yf = self.getLine(distance, angle) 
        #print(xf, yf)
        cv2.line(image,
            (x, y),
            (x+xf, y+yf),
            color,
            thickness=2,
            lineType=cv2.LINE_AA)

    def drawCone(self, image, x, y, distance, angleIn, angleFin):
        for i in range(angleFin-angleIn):
            for a in range(4):
                self.drayLine(image, x, y, distance, i+angleIn+a/4, (200, 200, 200))

    def semiCircle(self, image, x, y, distance, angleIn, angleFin):
        for i in range(angleFin-angleIn):
            for a in range(4):
                xf , yf = self.getLine(distance, i+angleIn+a/4)
                cv2.circle(image, (x+xf,y+yf), radius=0, color=(100, 100, 100), thickness=-1)

    def getLine(self, distance, angle):
        angle = -angle * np.pi / 180
        x = int(np.cos(angle)*distance)
        y = int(np.sin(angle)*distance)
        return x, y