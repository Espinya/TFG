from flask import Flask, render_template, Response

from Radar import Radar




if __name__ == "__main__":
    ip = "192.168.1.2"

    app = Flask(__name__)
    detection = Radar(ip=ip)
    @app.route('/')
    def index():
        return render_template('index.html')
    
    @app.route('/video_feed')
    def video_feed():
        return Response(detection.getImage(), mimetype='multipart/x-mixed-replace; boundary=frame')

    app.run(debug=True, use_reloader=False, threaded=True, host=ip)
    
    
    
  
    