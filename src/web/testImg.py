

import numpy as np
import cv2
from time import sleep

def getLine(distance, angle):
    angle = -angle * 2 * np.pi / 360
    x = int(np.cos(angle)*distance)
    y = int(np.sin(angle)*distance)
    return x, y


def drayLine(image, x, y, distance, angle, color=(0,0,0)):
    xf, yf = getLine(distance, angle) 
    #print(xf, yf)
    cv2.line(image,
        (x, y),
        (x+xf, y+yf),
        color,
        thickness=2,
        lineType=cv2.LINE_AA)




imageNormal = np.zeros([500, 500,3],dtype=np.uint8)
imageNormal.fill(255)








angle = 0
while(True):
    drayLine(imageNormal, 250, 250, distance=150, angle=angle, color=(0, 0, 0))
    cv2.imshow("Frame", imageNormal)
    cv2.waitKey(1)
    angle += 10
    print(angle)
    sleep(1)