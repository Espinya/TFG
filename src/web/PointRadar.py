import cv2
import numpy as np


class PointRadar:
    def __init__(self, cameraX = 0, cameraY = 0, cameraAngle = 0, distance = 0, theta = 0, status = 0, ref = 0):
        self.status = status
        #Status 0 normal situation, 1 warning posible fall accident
        self.time = 1
        self.ref = ref
        self.x , self.y = self.setPosition(
            cameraX=cameraX,
            cameraY = cameraY ,
            cameraAngle = cameraAngle,
            distance = distance,
            theta = theta)
        if self.status == 1:
            self.locked = True
        else:
            self.locked = False
        
    def setPosition(self, cameraX, cameraY, cameraAngle, distance, theta):
    

        angle = -(cameraAngle + theta) * np.pi / 180


        sin = np.sin(angle)
        cos = np.cos(angle)

        newX = cos*distance
        newY = sin*distance
        
        posX = cameraX + newX
        posY = cameraY + newY
        newX = int(self.ref*posX/100)
        newY = int(self.ref*posY/100)
        return newX, newY

    def pointCondition(self):
        otherColor = self.time * 50
        if self.status == 0:
            color = (otherColor, 255, otherColor)
        elif self.status == 1:
            color = (otherColor, otherColor, 255)
        elif self.status == 2:
            color = (255, otherColor, otherColor)
        else:
            color = (0, 0, 0)
        if self.locked == False:
            self.time +=1
        return color

    def canDestroyPoint(self):
        if (self.time > 3):
            return True
        else:
             return False
    def unlock(self):
        self.locked = False

    def draw(self, image):
        color = self.pointCondition()
        cv2.circle(image, (self.x, self.y), int(self.ref/15), color, 5)