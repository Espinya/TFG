import threading
import cv2
import numpy as np
from time import sleep
import pickle
from CameraRadar import CameraRadar
from LegendRadar import LegendRadar
from PointRadar import PointRadar

import threading

import socket


class Radar:
    def __init__(self, resolution = 1000, ip = "127.0.0.1"):
        self.__width = resolution
        self.__height = resolution
        self.__ref = int(resolution/20)
        self.__cameras = {}
        self.__points = []
        self.__dataBuffer = []
        self.__imageNormal = None
        self.__imageComplete = None
        self.__opcode = 0
        self.legend = LegendRadar(self.__ref)
        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
        self.__socket.bind((ip, 2500))
        self.__socket.setblocking(True)
        self.defaultRadar()
        self.startThreadImage()
        self.startThreadData()
        
    def clearImage(self):
        self.__imageNormal = np.zeros([self.__height, self.__width,3],dtype=np.uint8)
        self.__imageNormal.fill(255)



    def defaultRadar(self):
        self.clearImage()
        for camera in self.__cameras:
            self.__cameras[camera].draw(self.__imageNormal)
        self.legend.draw(self.__imageNormal)


    def checkCameras(self, data):
        if data[0] in self.__cameras:
            if not self.__cameras[data[0]].checkChanges(data):
                return False
        self.__cameras[data[0]] = CameraRadar(data[1], data[2], data[3], self.__ref)
        return True

    def addPoints(self, data):
        canUnlock = False
        for i in range(data[4]):
            if data[7+i*3] == 0:
                canUnlock = True
            self.__points.append(PointRadar(cameraX = data[1], cameraY = data[2], cameraAngle = data[3], 
                distance = data[5+i*3], theta = data[6+i*3], status = data[7+i*3], ref=self.__ref))
        if canUnlock == True:
            self.unlockPoints()
    
    def unlockPoints(self):
        for point in self.__points:
            point.unlock()

    def startThreadImage(self):
        thread = threading.Thread(target=self.generateImage, args=())
        thread.setDaemon(True)
        thread.start()

    def startThreadData(self):
        thread = threading.Thread(target=self.getData, args=())
        thread.setDaemon(True)
        thread.start()

    def getData(self):
        while(True):
            data, address = self.__socket.recvfrom(4096)
            data = pickle.loads(data)
            self.__dataBuffer.append(data)
    
    def generateImage(self):
        self.defaultRadar()
        self.__imageComplete = self.__imageNormal.copy()
        while(True):
            while(len(self.__dataBuffer) > 0):
                data = self.__dataBuffer.pop(0)
                if self.checkCameras(data):
                    self.defaultRadar()
                self.addPoints(data)
            self.__imageComplete = self.__imageNormal.copy()
            numPoints = len(self.__points)
            for i in range(numPoints):
                a = numPoints-i-1
                self.__points[a].draw(self.__imageComplete)
                if self.__points[a].canDestroyPoint():
                    self.__points.pop(a)
            self.__opcode = self.plusOpcode(self.__opcode)
            sleep(0.25)
    
    def plusOpcode(self, opcode):
        if opcode >= 100:
            return 0
        else:
            return opcode +1

    def getImage(self):
        localOpcode = 0
        while(True):
            if localOpcode <= self.__opcode or (self.__opcode == 0 and localOpcode != 1 ):
                localOpcode = self.__opcode + 1
                ret, jpeg = cv2.imencode('.jpg', self.__imageComplete )
                frame = jpeg.tobytes()
                yield (b'--frame\r\n'
                    b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')
    

   
        

    def img (self):
        cv2.imshow('Radar',  self.__imageComplete)
        cv2.waitKey(1)
        


if __name__ == "__main__":


    detection = Radar()
    while(True):
        sleep(1)
        detection.img()
       
    