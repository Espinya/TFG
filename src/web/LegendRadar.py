import cv2
import numpy as np

class LegendRadar:
    def __init__(self, ref = 0):
        self.__ref = ref
    
    def drawLineH(self, image):
        cv2.line(image,
            (self.__ref*20, self.__ref*1),
            (self.__ref*20, self.__ref*6),
            color=(0,0,0),
            thickness=4,
            lineType=cv2.LINE_AA)

    def drawSubLines(self, image):
        for i in range(6):
            cv2.putText(image, "{}m".format(i), (self.__ref*18, self.__ref*(i+1)),
                cv2.FONT_HERSHEY_TRIPLEX, 0.8, (200, 200, 200), 1)
            cv2.line(image,
            (self.__ref*19, self.__ref*(i+1)),
            (self.__ref*20, self.__ref*(i+1)),
            color=(0,0,0),
            thickness=1,
            lineType=cv2.LINE_AA)

    def drawCases(self, image):
        cv2.putText(image, "Ok", (int(self.__ref*19), self.__ref*7),
            cv2.FONT_HERSHEY_TRIPLEX, 0.8, (100, 100, 100), 1)
        cv2.putText(image, "Fall", (int(self.__ref*19), self.__ref*8),
            cv2.FONT_HERSHEY_TRIPLEX, 0.8, (100, 100, 100), 1)
        cv2.circle(image, (int(self.__ref*18.7), int(self.__ref*6.9)), int(self.__ref/15), (0, 255, 0), 5)
        cv2.circle(image, (int(self.__ref*18.7), int(self.__ref*7.9)), int(self.__ref/15), (0, 0, 255), 5)

    def draw(self, image):



        self.drawSubLines(image)
        self.drawLineH(image)
        self.drawCases(image)