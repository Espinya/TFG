#Fuentes consultadas
#https://picamera.readthedocs.io/en/release-1.10/recipes1.html
#https://stackoverflow.com/questions/53576851/socket-programming-in-python-using-pickle
#http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_gui/py_video_display/py_video_display.html


#https://github.com/quanhua92/human-pose-estimation-opencv
#https://www.pyimagesearch.com/2015/12/28/increasing-raspberry-pi-fps-with-python-and-opencv/


import socket
import pickle
import threading
from time import sleep
from SSH import SSH
from Video import Video


class CameraInterface:
    def __init__(self, mode = "LOAD", ip="192.168.1.3", port = 8005, resolution = (640,480), tcpBuffSize=8192000, fileName="CameraData.dat", debug=False, nCaptures = 10):
        #Recomended  res 1920*1088 or 640*480
        self.__mode = mode
        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__socket.setblocking(True)
        self.__haveConnection = False
        self.__ip = ip
        self.__port = port
        self.__debug = debug
        self.__nCaptures = nCaptures
        self.__dataFrame = b""
        self.__dataFrameBuffer = []
        self.__resolution = resolution
        self.__imgBuffSize = self.__resolution[0]*self.__resolution[1]*3 + 164
        self.__tcpBuffSize = tcpBuffSize
        self.__fileName = fileName
        self.__raspberry3 = None
        self.__stop = False
        if self.__mode == "LOAD" or self.__mode == "STREAM":
            self.__video = Video(self.__debug)
    def start(self):
        if self.__mode == "STREAM":
            self.__raspberry3 = SSH()
            self.__raspberry3.runPythonScript(script="RaspberryPiStream.py -port {} -resX {} -resY {} -tcpBuffSize {}".format(self.__port, self.__resolution[0], self.__resolution[1], self.__tcpBuffSize))
        elif self.__mode == "SAVE":
            self.__raspberry3 = SSH()
            self.__raspberry3.runPythonScript(script="RaspberryPiRecord.py -port {} -resX {} -resY {} -tcpBuffSize {} -nCaptures {}".format(self.__port, self.__resolution[0], self.__resolution[1], self.__tcpBuffSize, self.__nCaptures))
        self._startMainThread()
        if self.__debug == True:
           print("Starting...")

    def close(self):
        if self.__stop == False:
            #Añadir stop en el python de la raspberry
            self.__stop = True
            self.__video.stop()
            if self.__debug == True:
                print("Closing...")
        
    def _startMainThread(self):
       thread = threading.Thread(target=self._mainThread, args=())
       thread.setDaemon(True)
       thread.start()
       if self.__debug == True:
            print("Thread Running")

    def _startSocketThread(self):
       thread = threading.Thread(target=self._socketThread, args=())
       thread.setDaemon(True)
       thread.start()
       if self.__debug == True:
           print("Thread Running")
    
    def _mainThread(self):
        if self.__mode == "STREAM": #Stream from camera
            self._startConnection()
            self._startSocketThread()
            while(True):
                if self.__stop == True:
                    return
                self._showFrame()
        elif self.__mode == "SAVE": #Save from camera
            self._startConnection()
            self._socketStoreData()
            self._saveFrames()
            if self.__debug == True:
                print("Data captured")
                return       
        elif self.__mode == "LOAD": #Load from stored files
            self._loadFrames()
            while(True):
                if self.__stop == True:
                    return
                self._getFrameFromBuffer()
                self._showFrame()
                sleep(0.25) 
        else:
            raise Exception("Unknow mode")

    def _startConnection(self):
        self.__socket.connect((self.__ip, self.__port))
        self.__socket.setblocking(True)
        self.__haveConnection = True
        if self.__debug == True:
            print("Connection Accepted")
    def _closeConnection(self):
        if self.__haveConnection == True:
            self.__haveConnection = False
            self.__socket.close()
            if self.__debug == True:
                print("Connection Closed")


    def _socketThread(self):
        try:
            while(True):
                packetData = b""
                while(len(packetData) != self.__imgBuffSize):
                    packetData += self.__socket.recv(self.__tcpBuffSize)
                    if self.__stop == True:
                        return
                self.__dataFrame = packetData
                self.__socket.sendall(b'1')
        except Exception as e:
            try:
                self._closeConnection()
            except:
                pass
            if self.__debug == True:
                print("Error in Socket Thread", e)
            self.__stop()
      
    def _sendFrameToBuffer(self):
        if self.__dataFrame != b"":
            self.__dataFrameBuffer.append(self.__dataFrame)

    def _getFrameFromBuffer(self):
        if len(self.__dataFrameBuffer) == 0:
            self.close()
        else:
            self.__dataFrame = self.__dataFrameBuffer.pop(0)
    def _showFrame(self):
        try:
            if self.__dataFrame != b"":
                image = pickle.loads(self.__dataFrame)
                self.__dataFrame = b""
                self.__video.display(image)
        except:
            if self.__debug == True:
                print("excep", len(self.__dataFrame))
  
    def _socketStoreData(self):
        try:
            while(True):
                packetData = b""
                while(len(packetData) != self.__imgBuffSize * self.__nCaptures):
                    packetData += self.__socket.recv(self.__tcpBuffSize)
                    if self.__stop == True:
                        return
                self.__dataFrameBuffer =  pickle.load(packetData)
                self.__socket.sendall(b'1')
                return
        except Exception as e:
            try:
                self._closeConnection()
            except:
                pass
            if self.__debug == True:
                print("Error in Socket Thread", e)
            self.__stop()


    def _saveFrames(self):
        try:
            if self.__debug == True:
                print("Source: {}".format("src/server/{}".format(self.__fileName)))
            file = open("src/server/{}".format(self.__fileName), "wb")
        except:
            file = open(self.__fileName, "wb")
        pickle.dump(self.__dataFrameBuffer, file=file)
    def _loadFrames(self):
        try:
            file = open("src/server/{}".format(self.__fileName), "rb")
            if self.__debug == True:
                print("Source: {}".format("src/server/{}".format(self.__fileName)))
        except:
            try:
                file = open(self.__fileName, "rb")
            except:
                self.close()
                if self.__debug == True:
                    raise Exception("Error, no file to load found.")
        try:
            self.__dataFrameBuffer = pickle.load(file)
        except:
            self.close()
            if self.__debug == True:
                raise Exception("Error in the format of the file.")
    @property
    def status(self):
        if self.__stop == True:
            return "OFF"
        elif self.__haveConnection == True:
            return "CONECTED"
        else:
            return "WAITING"




if __name__ == "__main__":
    cam0 = CameraInterface(mode = "SAVE", ip="192.168.1.3", debug=True, fileName="CameraData0.dat")
    cam0.start()
    

    while(True):
        pass
    #finally:
       # project.closeConnection()



