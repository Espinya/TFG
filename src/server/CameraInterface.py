#Fuentes consultadas
#https://picamera.readthedocs.io/en/release-1.10/recIPes1.html
#https://stackoverflow.com/questions/53576851/socket-programming-in-python-using-pickle
#http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_gui/py_video_display/py_video_display.html


#https://github.com/quanhua92/human-pose-estimation-opencv
#https://www.pyimagesearch.com/2015/12/28/increasing-raspberry-pi-fps-with-python-and-opencv/


import socket
import pickle
import threading
from time import sleep
from SSH import SSH
from Video import Video
from DataToWeb import DataToWeb


class CameraInterface:
    def __init__(self, mode = "STREAM", cameraIP="192.168.1.3", cameraPort = 8005, resolution = (640,480), webIP = "192.168.1.2", webPort = 2500, tcpBuffSize=8192000, fileName="CameraData.dat", debug=False, nCaptures= 200, cameraID = 0, cameraCords = (1000, 500, 50, 90)):
        #Recomended  res 1920*1088 or 640*480
        self.__mode = mode
        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__socket.setblocking(True)
        self.__haveConnection = False
        self.__IP = cameraIP
        self.__port = cameraPort
        self.__debug = debug
        self.__Frame = None
        self.__FrameBuffer = []
        self.__resolution = resolution
        self.__imgBuffSize = self.__resolution[0]*self.__resolution[1]*3 + 164
        self.__tcpBuffSize = tcpBuffSize
        self.__fileName = fileName
        self.__raspberry3 = None
        self.__stop = False
        self.__video = Video(self.__debug)
        self.__nCaptures = nCaptures
        self.__sendDataToWeb = DataToWeb(cameraID=cameraID, cameraCords=cameraCords, resolution=self.__resolution, IP=webIP, port=webPort)
    def start(self):
        if self.__mode == "STREAM":
            self.__raspberry3 = SSH()
            self.__raspberry3.runPythonScript(script="RaspberryPiStream.py -port {} -resX {} -resY {} -tcpBuffSize {} -nCaptures {}".format(self.__port, self.__resolution[0], self.__resolution[1], self.__tcpBuffSize, 0))
        if self.__mode == "RECORD":
            self.__raspberry3 = SSH()
            self.__raspberry3.runPythonScript(script="RaspberryPiStream.py -port {} -resX {} -resY {} -tcpBuffSize {} -nCaptures {}".format(self.__port, self.__resolution[0], self.__resolution[1], self.__tcpBuffSize, self.__nCaptures))
        self._startMainThread()
        if self.__debug == True:
           print("Starting...")

    def close(self):
        if self.__stop == False:
            #Añadir stop en el python de la raspberry
            self.__stop = True
            self.__video.saveVideo()
            if self.__debug == True:
                print("Closing...")
        
    def _startMainThread(self):
       thread = threading.Thread(target=self._mainThread, args=())
       thread.setDaemon(True)
       thread.start()
       if self.__debug == True:
            print("Thread Running")

    def _startSocketThread(self):
       thread = threading.Thread(target=self._socketThread, args=())
       thread.setDaemon(True)
       thread.start()
       if self.__debug == True:
           print("Thread Running")
    
    def _startConnection(self):
        self.__socket.connect((self.__IP, self.__port))
        self.__socket.setblocking(True)
        self.__haveConnection = True
        if self.__debug == True:
            print("Connection Accepted")
    def _closeConnection(self):
        if self.__haveConnection == True:
            self.__haveConnection = False
            self.__socket.close()
            if self.__debug == True:
                print("Connection Closed")


    def _socketThread(self):
        try:
            while(True):
                packetData = b""
                while(len(packetData) != self.__imgBuffSize):
                    packetData += self.__socket.recv(self.__tcpBuffSize)
                    if self.__stop == True:
                        return
                self.__Frame = packetData
                self.__FrameBuffer.append(self.__Frame)
                self.__socket.sendall(b'1')
        except Exception as e:
            try:
                self._closeConnection()
            except:
                pass
            if self.__debug == True:
                print("Error in Socket Thread", e)
            self.close()
      
    def _getFrameFromBuffer(self):
        if len(self.__FrameBuffer) == 0:
            self.close()
        else:
            self.__Frame = self.__FrameBuffer.pop(0)
    def _showFrame(self):
        if self.__Frame is not None:
            image = pickle.loads(self.__Frame)
            self.__Frame = None
            positionData = self.__video.display(image)
            return positionData
        
    def _haveEnoughtFrames(self):
        if len(self.__FrameBuffer) == self.__nCaptures:
            return True
        else:
            return False
    def _saveFrames(self):
        try:
            if self.__debug == True:
                print("Source: {}".format("src/server/out/{}".format(self.__fileName)))
            file = open("src/server/out/{}".format(self.__fileName), "wb")
        except:
            file = open(self.__fileName, "wb")
        pickle.dump(self.__FrameBuffer, file=file)
    def _loadFrames(self):
        try:
            file = open("src/server/out/{}".format(self.__fileName), "rb")
            if self.__debug == True:
                print("Source: {}".format("src/server/out/{}".format(self.__fileName)))
        except:
            try:
                file = open(self.__fileName, "rb")
            except:
                self.close()
                if self.__debug == True:
                    raise Exception("Error, no file to load found.")
        try:
            self.__FrameBuffer = pickle.load(file)
        except:
            self.close()
            if self.__debug == True:
                raise Exception("Error in the format of the file.")
    @property
    def status(self):
        if self.__stop == True:
            return "OFF"
        elif self.__haveConnection == True:
            return "CONECTED"
        else:
            return "WAITING"


    def _mainThread(self):
        if self.__mode == "STREAM": #Stream from camera
            self._startConnection()
            self._startSocketThread()
            while(True):
                if self.__stop == True:
                    return
                positionData = self._showFrame()
                self.__sendDataToWeb.sendData(positionData)
        elif self.__mode == "RECORD": #Save from camera
            self._startConnection()
            self._startSocketThread()
            while(True):
                if self.__stop == True:
                    return
                #positionData = self._showFrame()
                #self.__sendDataToWeb.sendData(positionData)
                if self._haveEnoughtFrames():
                    self._saveFrames()
                    if self.__debug == True:
                        print("Record Completed")
                    return
        elif self.__mode == "LOAD": #Load from stored files
            self._loadFrames()
            while(True):
                if self.__stop == True:
                    return
                self._getFrameFromBuffer()
                positionData = self._showFrame()
                self.__sendDataToWeb.sendData(positionData)

        else:
            raise Exception("Unknow mode")


if __name__ == "__main__":
    sleep(5)
    cam0 = CameraInterface(mode = "LOAD", cameraIP="192.168.1.3", debug=True, fileName="CameraData2.dat", nCaptures=200)
    cam0.start()
    

    while(True):
        pass
    #finally:
       # project.closeConnection()



