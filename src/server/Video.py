import cv2
import numpy as np
from time import time
from recogition_algorithms.FaceRecognition import FaceRecognition
from recogition_algorithms.MediaPipe import MediaPipe
from recogition_algorithms.dnn import DnnFaceRecognition
#from recogition_algorithms.mtcnn import MtcnnFaceRecognition
class Video:
    def __init__(self, debug = False, resolution = (640, 480)):
        self.__debug = debug
        self.__times = 0
        self.__timesOld = time()
        self.__frameBuffer = []
        self.__frame = None
        self.__manipulator = DnnFaceRecognition()
        self.__resolution = resolution


    def saveVideo(self):
        if len(self.__frameBuffer) > 0:
            cv2.destroyAllWindows()
            fourcc = cv2.VideoWriter_fourcc(*"MP4V")
            out = cv2.VideoWriter("src/server/out/out.mp4", fourcc, 4, self.__resolution)
            while True:
                if len(self.__frameBuffer) != 0:
                    out.write(self.__frameBuffer.pop(0))
                else:
                    out.release()
                    return
    
    def _displayFrame(self):
        cv2.imshow("Frame", self.__frame)
        cv2.waitKey(1)

    def _printFps(self):
        if self.__debug == True:
            self.__times = time()
            print("FrameNum: {} Fps: {}".format( len(self.__frameBuffer), 1/(self.__times-self.__timesOld)))
            self.__timesOld = self.__times

    def display(self, frame):
        self.__frame = frame.copy()
        #self.__frame = cv2.rotate(self.__frame , cv2.cv2.ROTATE_180)
        self.__frame = cv2.resize(self.__frame  , self.__resolution)
        #self.__frame = cv2.cvtColor(self.__frame, cv2.COLOR_BGR2RGB)
        self.__frame, rectangleList = self.__manipulator.process(self.__frame)
        self.__frameBuffer.append(self.__frame)
        if self.__debug == True:
            self._displayFrame()
            self._printFps()
            print("Rectangles", rectangleList)
        return rectangleList






