from mtcnn import MTCNN
import cv2
import numpy as np

#Best algorithm, but needs a gpu with cudas
#https://sefiks.com/2020/09/09/deep-face-detection-with-mtcnn-in-python/
class MtcnnFaceRecognition():
    def __init__(self):
        self.__detector = MTCNN()

    def process(self, img):
        positionData = []
        detections = self.__detector.detect_faces(img)
        for detection in detections:
            score = detection["confidence"]
            if score > 0.90:
                box = detection["box"]
                (x, y, x1, y1) = box
                x1 += x
                y1 += y
                #(x, y, x1, y1) = box.astype("int")
                cv2.rectangle(img, (x, y), (x1, y1), (0, 0, 255), 2)
                positionData.append((x, y, x1, y1))

        return img, positionData