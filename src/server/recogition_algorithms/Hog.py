"https://carlosjuliopardoblog.wordpress.com/2018/06/28/deteccion-de-peatones-con-python-y-opencv-hog-svm/"

import cv2

class DistanceEstimate():
    def __init__(self):
        pass



    def _initHog(self):
        self.__hog = cv2.HOGDescriptor()
        self.__hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())



    def _displayHog(self):
        #Credits https://thedatafrog.com/en/articles/human-detection-video/
        frame = cv2.rotate(self.__image, cv2.cv2.ROTATE_180)
        frame = cv2.resize(frame, (640, 480))
        # using a greyscale picture, also for faster detection
        gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)

        # detect people in the image
        # returns the bounding boxes for the detected objects
        boxes, weights = self.__hog.detectMultiScale(frame, winStride=(8,8) )
        boxes = np.array([[x, y, x + w, y + h] for (x, y, w, h) in boxes])
        pick = cv2.non_max_suppression_slow(boxes, 0.3)

        for (xA, yA, xB, yB) in pick:
            # display the detected boxes in the colour picture
            cv2.rectangle(frame, (xA, yA), (xB, yB),
                (0, 255, 0), 2)
        # Write the output video 
        #out.write(frame.astype('uint8'))
        # Display the resulting frame
        cv2.imshow('frame',frame)
        cv2.waitKey(1)


    def non_max_suppression_slow(boxes, overlapThresh):
	    # if there are no boxes, return an empty list
	    if len(boxes) == 0:
		    return []
	    # initialize the list of picked indexes
	    pick = []
	    # grab the coordinates of the bounding boxes
	    x1 = boxes[:,0]
	    y1 = boxes[:,1]
	    x2 = boxes[:,2]
	    y2 = boxes[:,3]
	    # compute the area of the bounding boxes and sort the bounding
	    # boxes by the bottom-right y-coordinate of the bounding box
	    area = (x2 - x1 + 1) * (y2 - y1 + 1)
	    idxs = np.argsort(y2)

