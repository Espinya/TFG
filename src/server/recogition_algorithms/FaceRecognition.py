#based https://www.geeksforgeeks.org/realtime-distance-estimation-using-opencv-python/
#https://omes-va.com/deteccion-de-rostros-con-haar-cascades-python-opencv/
import cv2
 

class FaceRecognition():
    def __init__(self):
        self.__frontalFaceData = cv2.CascadeClassifier("src/server/IAdata/haarcascade_frontalface_default.xml")
        self.__face_detector = cv2.CascadeClassifier("src/server/IAdata/haarcascade_frontalface_default.xml")
        #self.__profilefaceData = cv2.CascadeClassifier("src/server/IAdata/haarcascade_profileface.xml")
        self.__profilefaceData = cv2.CascadeClassifier("src/server/IAdata/haarcascade_frontalface_default.xml")
        self.__face = None
        
    def _profileFace(self, frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = self.__profilefaceData.detectMultiScale(image=gray,
            scaleFactor=1.10,
            minNeighbors=20,
            flags=cv2.CASCADE_SCALE_IMAGE,
            minSize=(20,20),
            maxSize=(100,100))
        if len(faces) > 0:
            return faces[0]
        return None
        for (x,y,w,h) in faces:
            cv2.rectangle(frame, (x,y),(x+w,y+h),(0,255,0),2)
        return frame
 
    def _frontalFace(self, frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = self.__frontalFaceData.detectMultiScale(image=gray,
            scaleFactor=1.10,
            minNeighbors=5,
            flags=cv2.CASCADE_SCALE_IMAGE,
            minSize=(20,20),
            maxSize=(100,100))
        if len(faces) > 0:
            return faces[0]
        return None
        for (x,y,w,h) in faces:
            cv2.rectangle(frame, (x,y),(x+w,y+h),(0,255,0),2)
        return frame

    def _drawFace(self, frame, face):
        x, y, w, h = face
        cv2.rectangle(frame, (x,y),(x+w,y+h),(0,255,0),2)
        return frame



    def modifyFrame2(self, frame):
        face = self._frontalFace(frame)
        if face is not None:
            self._drawFace(frame, face)
            return frame
        face = self._profileFace(frame)
        if face is not None:
            self._drawFace(frame, face)
            return frame

        return frame
 

    def modifyFrame(self, frame):
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = self.__profilefaceData.detectMultiScale(image=frame,
            scaleFactor=1.01,
            minNeighbors=10,
            flags=cv2.CASCADE_SCALE_IMAGE,
            minSize=(20,20),
            maxSize=(100,100))
        for (x,y,w,h) in faces:
            cv2.rectangle(frame, (x,y),(x+w,y+h),(255,0,0),2)
        return frame



   
if __name__ == "__main__":
    from time import sleep
    print("PATH:", "src/server/examples/{}".format("1.jpg"))
    image = cv2.imread("src/server/examples/{}".format("1.jpg"))
    cv2.imshow("Frame", image)
    cv2.waitKey(1)
    sleep(2)
    faceRec = FaceRecognition()
    faceRec.modifyFrame(frame=image)
    cv2.imshow("Frame", image)
    cv2.waitKey(1)
    pass