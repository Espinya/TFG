import cv2
import mediapipe as mp
import threading
#https://google.github.io/mediapipe/solutions/pose#python-solution-api
#https://omes-va.com/deteccion-de-rostros-mediapipe-python/

class MediaPipe():
    def __init__(self, resolution=(640, 480)):
        self.__width = resolution[0]
        self.__height = resolution[1]
        self.__frame = None
        self.__start = False
        self.__end = False




        thread1 = threading.Thread(target=self.faceDetector, args=())
        thread1.setDaemon(True)
        thread1.start()

        #thread2 = threading.Thread(target=self.bodyDetector, args=())
        #thread2.setDaemon(True)
        #thread2.start()


    def drawRightEye(self, image, detection):
        # Ojo derecho
        x_RE = int(detection.location_data.relative_keypoints[0].x * self.__width)
        y_RE = int(detection.location_data.relative_keypoints[0].y * self.__height)
        cv2.circle(image, (x_RE, y_RE), 3, (0, 0, 255), 3)

    def drawLeftEye(self, image, detection):
        # Ojo izquierdo
        x_LE = int(detection.location_data.relative_keypoints[1].x * self.__width)
        y_LE = int(detection.location_data.relative_keypoints[1].y * self.__height)
        cv2.circle(image, (x_LE, y_LE), 3, (255, 0, 255), 3)

    def drawNoise(self, image, detection):
        # Punta de la nariz
        x_NT = int(detection.location_data.relative_keypoints[2].x * self.__width)
        y_NT = int(detection.location_data.relative_keypoints[2].y * self.__height)
        cv2.circle(image, (x_NT, y_NT), 3, (255, 0, 0), 3)
   
    def drawMouth(self, image, mp_face_detection, detection):
        # Centro de la boca
        x_MC = int(mp_face_detection.get_key_point(detection, mp_face_detection.FaceKeyPoint.MOUTH_CENTER).x * self.__width)
        y_MC = int(mp_face_detection.get_key_point(detection, mp_face_detection.FaceKeyPoint.MOUTH_CENTER).y * self.__height)
        cv2.circle(image, (x_MC, y_MC), 3, (0, 255, 0), 3)
    
    def drawRightEar(self, image, mp_face_detection, detection):
        # Oreja derecha
        x_RET = int(mp_face_detection.get_key_point(detection, mp_face_detection.FaceKeyPoint.RIGHT_EAR_TRAGION).x * self.__width)
        y_RET = int(mp_face_detection.get_key_point(detection, mp_face_detection.FaceKeyPoint.RIGHT_EAR_TRAGION).y * self.__height)
        cv2.circle(image, (x_RET, y_RET), 3, (0, 255, 255), 3)

    def drawLeftEar(self, image, mp_face_detection, detection):
        # Oreja izquierda
        x_LET = int(mp_face_detection.get_key_point(detection, mp_face_detection.FaceKeyPoint.LEFT_EAR_TRAGION).x * self.__width)
        y_LET = int(mp_face_detection.get_key_point(detection, mp_face_detection.FaceKeyPoint.LEFT_EAR_TRAGION).y * self.__height)
        cv2.circle(image, (x_LET, y_LET), 3, (255, 255, 0), 3)

    def drawFace(self, image, detection):
        # Bounding Box
        print(int(detection.location_data.relative_bounding_box.xmin * self.__width))
        xmin = int(detection.location_data.relative_bounding_box.xmin * self.__width)
        ymin = int(detection.location_data.relative_bounding_box.ymin * self.__height)
        w = int(detection.location_data.relative_bounding_box.width * self.__width)
        h = int(detection.location_data.relative_bounding_box.height * self.__height)
        cv2.rectangle(image, (xmin, ymin), (xmin + w, ymin + h), (0, 255, 0), 2)



    def faceDetector(self):
        mp_face_detection = mp.solutions.face_detection
        mp_drawing = mp.solutions.drawing_utils
        with mp_face_detection.FaceDetection(
            model_selection=0,
            min_detection_confidence=0.5) as face_detection:
            while(True):
                while(self.__start == False):
                    pass
                
                self.__start = False

                image = cv2.cvtColor(self.__frame, cv2.COLOR_BGR2RGB)
                # To improve performance, optionally mark the image as not writeable to
                # pass by reference.
                image.flags.writeable = False
                results = face_detection.process(image)

                image.flags.writeable = True
                image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

                if results.detections:
                    for detection in results.detections:
                        mp_drawing.draw_detection(image, detection)
                cv2.imshow('MediaPipe Face Detection', image)
                cv2.waitKey(1)
                """
                results = face_detection.process(cv2.cvtColor(self.__frame, cv2.COLOR_BGR2RGB))
                print("Detections:", results.detections)
                if results.detections is not None:
                    for detection in results.detections:
                        self.drawFace(image=self.__frame , detection=detection)
                        self.drawRightEye(image=self.__frame , detection=detection)
                        self.drawLeftEye(image=self.__frame , detection=detection)
                        self.drawNoise(image=self.__frame , detection=detection)
                        self.drawMouth(image=self.__frame , mp_face_detection=mp_face_detection, detection=detection)
                        self.drawRightEar(image=self.__frame , mp_face_detection=mp_face_detection, detection=detection)
                        self.drawLeftEar(image=self.__frame , mp_face_detection=mp_face_detection, detection=detection)
                """
                self.__end = True

    def process(self, frame):
        self.__frame = frame
        self.__start = True
        while(self.__end == False):
            pass
        self.__end = False
        #cv2.imshow("Image", frame)
        #cv2.waitKey(1)
            




