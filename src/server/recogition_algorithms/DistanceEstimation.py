#based https://www.geeksforgeeks.org/realtime-distance-estimation-using-opencv-python/
#https://omes-va.com/deteccion-de-rostros-con-haar-cascades-python-opencv/
import cv2
 

class DistanceEstimate():
    def __init__(self):
        # distance from camera to object(face) measured
        # centimeter
        self.Known_distance = 56.2
        # width of face in the real world or Object Plane
        # centimeter
        self.Known_width = 14.3
        # Colors
        self.GREEN = (0, 255, 0)
        self.RED = (0, 0, 255)
        self.WHITE = (255, 255, 255)
        self.BLACK = (0, 0, 0)
        # defining the fonts
        self.fonts = cv2.FONT_HERSHEY_COMPLEX
        # face detector object
        self.face_detector = cv2.CascadeClassifier("src/server/VideoProjects/haarcascade_frontalface_default.xml")
       
        self.ref_image = None
        self.Focal_length_found = None
        self.getFocal()
        
    def getFocal(self):
        # reading reference_image from directory
        self.ref_image = cv2.imread("src/server/VideoProjects/ref.png")
        # find the face width(pixels) in the reference_image
        ref_image_face_width = self.face_data(self.ref_image)
        # get the focal by calling "Focal_Length_Finder"
        # face width in reference(pixels),
        # Known_distance(centimeters),
        # known_width(centimeters)
        self.Focal_length_found = self.Focal_Length_Finder(self.Known_distance, self.Known_width,
            ref_image_face_width)


    # focal length finder function
    def Focal_Length_Finder(self, measured_distance, real_width, width_in_rf_image):
        # finding the focal length
        focal_length = (width_in_rf_image * measured_distance) / real_width
        return focal_length
    # distance estimation function
    def Distance_finder(self, Focal_Length, real_face_width, face_width_in_frame):
        distance = (real_face_width * Focal_Length)/face_width_in_frame
        return distance
 
    def face_data(self, frame):
        face_width = 0  # making face width to zero
        w = 0
        # converting color image ot gray scale image
        gray_image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # detecting face in the image
        faces = self.face_detector.detectMultiScale(gray_image, 
                scaleFactor=1.10,
                minNeighbors=20,
                flags=cv2.CASCADE_SCALE_IMAGE,
                minSize=(20,20),
        maxSize=(100,100))

  
        # looping through the faces detect in the image
        # getting coordinates x, y , width and height
        for (x, y, h, w) in faces:
            # draw the rectangle on the face
            cv2.rectangle(frame, (x, y), (x+w, y+h), self.GREEN, 2)
            # getting face width in the pixels
            face_width = w

        # return the face width in pixel
        return face_width
        
 
 
    def modifyFrame(self, frame):
        # calling face_data function to find
        # the width of face(pixels) in the frame
        face_width_in_frame = self.face_data(frame)
        # check if the face is zero then not
        # find the distance
        if face_width_in_frame != 0:
            # finding the distance by calling function
            # Distance distnace finder function need
            # these arguments the Focal_Length,
            # Known_width(centimeters),
            # and Known_distance(centimeters)
            self.Focal_length_found = 220
            distance = self.Distance_finder(
                self.Focal_length_found, self.Known_width, face_width_in_frame)
            # draw line as background of text
            cv2.line(frame, (30, 30), (230, 30), self.RED, 32)
            cv2.line(frame, (30, 30), (230, 30), self.BLACK, 28)
 
            # Drawing Text on the screen
            cv2.putText(
                frame, f"Distance: {round(distance,2)} CM", (30, 35),
                self.fonts, 0.6, self.GREEN, 2)
        return frame
 
   
if __name__ == "__main__":
    manipulator = DistanceEstimate()