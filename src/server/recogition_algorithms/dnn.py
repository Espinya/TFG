import cv2
import numpy as np

#https://github.com/opencv/opencv/tree/master/samples/dnn/face_detector
#https://github.com/mayank8200/Deep-Learning-Face-Detection
class DnnFaceRecognition():
    def __init__(self):
        modelFile = "src/server/models/res10_300x300_ssd_iter_140000.caffemodel"
        configFile = "src/server/models/deploy.prototxt.txt"
        self.__net = cv2.dnn.readNetFromCaffe(configFile, modelFile)

    def process(self, img):
        h, w = img.shape[:2]
        blob = cv2.dnn.blobFromImage(cv2.resize(img, (300, 300)), 1.0,
            (300, 300), (104.0, 117.0, 123.0))
        self.__net.setInput(blob)
        faces = self.__net.forward()
        positionData = []
        #to draw faces on image
        for i in range(faces.shape[2]):
            confidence = faces[0, 0, i, 2]
            if confidence > 0.5:
                box = faces[0, 0, i, 3:7] * np.array([w, h, w, h])
                (x, y, x1, y1) = box.astype("int")
                cv2.rectangle(img, (x, y), (x1, y1), (0, 0, 255), 2)
                positionData.append((x, y, x1, y1))
        return img, positionData