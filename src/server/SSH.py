
import socket
import pickle
from time import sleep

from paramiko import SSHClient, AutoAddPolicy





class SSH:
    def __init__(self, host="192.168.1.3", port = 22, username = "pi", password = "Rasp3", debug = False):
        "Clase para realizar una conexión ssh a través del python para ejecutar el nodo de la camara"
        self.__ssh = SSHClient()#Instanciamos el objeto para realizar la conexión ssh
        self.__ssh.set_missing_host_key_policy(AutoAddPolicy())
        self.__ssh.connect(host, port, username, password)#Procedemos a hacer la conexión con el servidor remoto
        self.__debug = debug

    def sendCommand(self, command):
        "Método para enviar un comando de ssh"
        stdin, stdout, stderr = self.__ssh.exec_command(command)
        if self.__debug == True:
            print("Response: {}".format(stdout))
        return stdout
    def runPythonScript(self, script = "raspberryPiCamera.py"):
        "Método para ejecutar un script de python remoto"
        self.sendCommand("screen -S pythonscript -p 0 -X quit")#Cerramos el script si todavía seguía ejecutandose
        if self.__debug == True:
             print("screen -S pythonscript -p 0 -X quit")
        self.sendCommand("chmod a+x ~/workspace/{}".format(script))#Damos permisos para que el script sea ejecutable
        if self.__debug == True:
             print("Command: chmod a+x ~/workspace/{}".format(script))
        self.sendCommand("screen -S pythonscript -d -m python3 ~/workspace/{}".format(script))#Ejecutamos el script con el python 3
        if self.__debug == True:
            print("Command: screen -S pythonscript -d -m python3 ~/workspace/{}".format(script))


if __name__ == "__main__":
    client0 = SSH(debug=True)
    client0.runPythonScript()
    



