import pickle
import socket
from time import time,sleep
from threading import Thread

class DataToWeb:
    def __init__(self, cameraID = 0, cameraCords = (1000, 1000, 270), IP = "127.0.0.1", port = 2500, resolution = (640,480)):
        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # UDP
        self.__cameraID = cameraID
        self.__cameraX = cameraCords[0]
        self.__cameraY = cameraCords[1]
        self.__cameraAngle = cameraCords[2]
        self.__port = port
        self.__resolution = resolution
        self.__IP = IP
        self.__cameraFOV = 60 #Max angle of the camera
        self.__refDistance = 11000 #Ref value to detect the image in 1 m
        self.socket = self.__socket

        self.__ownStatus = 0
        self.__fallConfiance = 300
        self.__yPast = 0
        self.__statusTime = time()
        #esperar 3s, durante 5s comprovar si está en el suelo, si no se detecta nada,saltar alarma
        #time elapsed



    def sendData(self, pointsData):
        if pointsData is not None:
            if len(pointsData) > 0:
                data = self.generatePacket(pointsData)
                self.__socket.sendto(data, (self.__IP, self.__port))

    def getDistance(self, x,y,x1,y1):
        distance = (y1-y)
        return int(self.__refDistance/distance)

    def getTheta(self, x,y,x1,y1):
        theta = (x+x1)/(2*self.__resolution[0])
        return int((self.__cameraFOV*(1-theta))-self.__cameraFOV/2)


    def threadStatusPromisse(self, distance, theta):
        thread = Thread(target=self._mainThread, args=(self, distance, theta))
        thread.setDaemon(True)
        thread.start()

    def sendPromisse(self, distance, theta):#Promesa para evitar falsos positivos
        sleep(5)#Si después de detectar la caida, se espera 3s y en los 2s posteriores no se detecta nadie de pie, enviar alarma
        if self.__ownStatus == 2:
            data = []
            data.append(self.__cameraID)
            data.append(self.__cameraX)
            data.append(self.__cameraY)
            data.append(self.__cameraAngle)
            data.append(1)
            data.append(distance)
            data.append(theta)
            data.append(1)
            buffer = pickle.dumps(data)
            self.__ownStatus = 0


    def setStatus(self, y1, distance):
        if self.__ownStatus == 0:
            if time() - self.__statusTime  > 2:
                self.__yPast = 0
            self.__statusTime = time() 
            fallRef = distance*self.__resolution[1]
            yPresent = fallRef * y1
            print("dataa", (self.__yPast - yPresent)/10000)
            if self.__yPast != 0:
                if (yPresent - self.__yPast) > self.__fallConfiance*10000:
                    self.__ownStatus = 1
                    self.__statusTime = time()
                    self.__yPast = yPresent
            self.__yPast = yPresent
        
        elif self.__ownStatus == 1: #Filtro anti-rebotes, esperamos 3s después de detectar la caida
            if time() - self.__statusTime  > 3:
                self.__ownStatus = 2
        elif self.__ownStatus == 2:#Si pasado estos segundos, detectamos actividad
            if y1/self.__resolution[1] < 0.7:#Si no está en el suelo, consideramos que es falsa alarma
                self.__ownStatus = 0
        return 0

    def getDataPoints(self, rectangle):
        x,y,x1,y1 = rectangle
        distance = self.getDistance(x,y,x1,y1)
        theta = self.getTheta(x,y,x1,y1)
        self.setStatus(y1, distance)
        print("DISTANCE: ", distance, "THETA: ", theta)
        return distance,theta

    def generatePacket(self, rectangleList):
        num = len(rectangleList)
        data = []
        data.append(self.__cameraID)
        data.append(self.__cameraX)
        data.append(self.__cameraY)
        data.append(self.__cameraAngle)
        data.append(num)
        for rectangle in rectangleList:
            distance, theta = self.getDataPoints(rectangle)
            data.append(distance)
            data.append(theta)
            data.append(0)

        buffer = pickle.dumps(data)
        return buffer

   

if __name__ == "__main__":

    sendData = DataToWeb()
    while(True):
        data = [(100, 20, 0)]
        sendData.sendData(data)
        sleep(0.5)
        data = [(250, 20, 0)]
        sendData.sendData(data)
        sleep(0.5)
        data = [(400, 20, 0)]
        sendData.sendData(data)
        sleep(0.5)
        data = [(500, 20, 0)]
        sendData.sendData(data)
        sleep(5)

    