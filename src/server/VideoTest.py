import cv2
import numpy as np
from time import sleep
from videoprojects.FaceRecognition import FaceRecognition
from videoprojects.DistanceEstimate import DistanceEstimate
 

class VideoTest():
    def __init__(self, examplesStart = 0, examplesNum = 0):
        self.__examplesList = []
        self.__examplesStart = examplesStart
        self.__examplesNum = examplesNum
        self.loadAllExamples()
        self.__manipulator = FaceRecognition()

    def loadAllExamples(self):
        for i in range(self.__examplesNum):
            self.loadExamples("{}.jpg".format(str(self.__examplesStart+i)))

    def loadExamples(self, name):
        print("PATH:", "src/server/examples/{}".format(name))
        image = cv2.imread("src/server/examples/{}".format(name))
        if image is not None:
            self.__examplesList.append(image)

   
    def displayExample(self, example):
        self.__manipulator.modifyFrame(example)
        cv2.imshow("Frame", example)
        cv2.imshow("Frame2", example)
        cv2.waitKey(1)

    def test(self):
        for example in self.__examplesList:
            example = cv2.resize(example  , (640 , 480))
            self.displayExample(example)
            sleep(3)

if __name__ == "__main__":
    videoTest = VideoTest(examplesStart=1, examplesNum=14)
    videoTest.test()



  