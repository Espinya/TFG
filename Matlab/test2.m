rgbImage = imread('detectcirclesexample_02_es.png', 'png');

org = rgbImage
grayImage = rgb2gray(rgbImage);
borderImage = edge(grayImage,'Prewitt'); %Aplicamos un filtro de bordes
cleanImage = bwareaopen(borderImage,10); %Eliminamos el "ruido, es decir los cuerpos con pocos pixels
subplot(1,2,1);
imshow(org)

se = strel('disk',12);
bw = imclose(cleanImage,se);
subplot(1,2,2);
imshow(org)
bw = imfill(bw,'holes');

imshow(org);

[B,L] = bwboundaries(bw,'noholes');

imshow(label2rgb(L,@jet,[.5 .5 .5]))
hold on 
for k = 1:length(B)
    boundary = B{k};
    plot(boundary(:,2),boundary(:,1),'w','LineWidth',2)
end
stats = regionprops(L,'Area','Centroid');
threshold = 0.94;
% loop over the boundaries 
for k = 1:length(B)
    % obtain (X,Y) boundary coordinates corresponding to label 'k'   
    boundary = B{k};    % compute a simple estimate of the object's perimeter   
    delta_sq = diff(boundary).^2;
    perimeter = sum(sqrt(sum(delta_sq,2)));
    % obtain the area calculation corresponding to label 'k'   
    area = stats(k).Area;      % compute the roundness metric   
    metric = 4*pi*area/perimeter^2;      % display the results   
    metric_string = sprintf('%2.2f',metric);    % mark objects above the threshold with a black circle   
   subplot(1,2,1);
    if metric > threshold
        text(boundary(1,2)+20,boundary(1,1)+40,metric_string,'Color','g','FontSize',14,'FontWeight','bold')
    else
        text(boundary(1,2)+20,boundary(1,1)+40,metric_string,'Color','r','FontSize',14,'FontWeight','bold')
    end
end
title(['Metrics closer to 1 indicate that ', 'the object is approximately round'])



