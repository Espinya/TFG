%https://es.mathworks.com/matlabcentral/answers/116793-how-to-classify-shapes-of-this-image-as-square-rectangle-triangle-and-circle

rgbImage = imread('humanoide.png', 'png');
greyImage = rgb2gray(rgbImage);
binareImage = imbinarize(greyImage);
subplot(1,2,1);
imshow(rgbImage)

binareImage = bwareaopen(binareImage,30);

binareImage = ~binareImage;
se = strel('disk',12);
binareImage = imclose(binareImage,se);

binareImage = imfill(binareImage,'holes');
subplot(1,2,2);
imshow(binareImage)

[B,L] = bwboundaries(binareImage,'noholes');

%imshow(label2rgb(L,@jet,[.5 .5 .5]))
hold on 
for k = 1:length(B)
    boundary = B{k};
    plot(boundary(:,2),boundary(:,1),'w','LineWidth',2)
end
stats = regionprops(L,'Area','Centroid');
threshold = 0.9;
% loop over the boundaries 
for k = 1:length(B)
    % obtain (X,Y) boundary coordinates corresponding to label 'k'   
    boundary = B{k};    % compute a simple estimate of the object's perimeter   
    delta_sq = diff(boundary).^2;
    perimeter = sum(sqrt(sum(delta_sq,2)));
    % obtain the area calculation corresponding to label 'k'   
    area = stats(k).Area;      % compute the roundness metric   
    metric = 4*pi*area/perimeter^2;      % display the results   
    metric_string = sprintf('%2.2f',metric);    % mark objects above the threshold with a black circle   
    if metric > threshold
        text(boundary(1,2)+20,boundary(1,1)+40,metric_string,'Color','g','FontSize',14,'FontWeight','bold')
    else
        text(boundary(1,2)+20,boundary(1,1)+40,metric_string,'Color','r','FontSize',14,'FontWeight','bold')
    end
end
%title(['Metrics closer to 1 indicate that ', 'the object is approximately round'])



