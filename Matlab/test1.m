rgbImage = imread('detectcirclesexample_02_es.png', 'png');
subplot(1,2,1);
imshow(rgbImage)
grayImage = rgb2gray(rgbImage);
borderImage = edge(grayImage,'Prewitt'); %Aplicamos un filtro de bordes
cleanImage = bwareaopen(borderImage,10); %Eliminamos el "ruido, es decir los cuerpos con pocos pixels
subplot(1,2,2);
imshow(cleanImage)
[centers,radii] = imfindcircles(cleanImage,[20, 60],'ObjectPolarity','dark','Sensitivity',0.87,'Method','twostage');
viscircles(centers, radii,'EdgeColor','b');

