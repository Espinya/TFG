rgb_image = imread('detectcirclesexample_02_es.png', 'png');
gray_image = rgb2gray(rgb_image);
imshow(rgb_image)



%,'EdgeThreshold',0.1
[centers,radii] = imfindcircles(gray_image,[20, 60],'ObjectPolarity','dark','Sensitivity',0.92,'Method','twostage');
[centers2,radii2] = imfindcircles(gray_image,[20, 60],'ObjectPolarity','bright','Sensitivity',0.92,'Method','twostage');



viscircles(centers, radii,'EdgeColor','b');
viscircles(centers2, radii2,'EdgeColor','b');